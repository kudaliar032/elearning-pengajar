<?php

use Illuminate\Database\Seeder;
use App\Module;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($x = 1;$x <= 5;$x++) {
            $module = new Module;
            $module->course_id = 1;
            $module->title = 'This is module name for module-'.$x;
            $module->module_content = 'This is content for module, lorem ipsum';
            $module->save();
        }
    }
}
