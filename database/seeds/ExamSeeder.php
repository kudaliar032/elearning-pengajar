<?php

use Illuminate\Database\Seeder;
use App\Exam;

class ExamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($x = 1; $x <= 10; $x++) {
            $exam = new Exam;
            $exam->course_id = 1;
            $exam->question = 'Pertanyaannya adalah '.$x;
            $exam->a = 'Jawaban a';
            $exam->b = 'Jawaban b';
            $exam->c = 'Jawaban c';
            $exam->d = 'Jawaban d';
            $exam->e = 'Jawaban e';
            $exam->answer = 'a';
            $exam->save();
        }
    }
}
