<?php

use Illuminate\Database\Seeder;
use App\Course;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse non arcu sodales, lobortis sem et, maximus enim. Vestibulum porttitor lorem at enim tristique feugiat. Maecenas eleifend feugiat metus, eu sagittis est facilisis id. Pellentesque tristique diam et turpis iaculis, non interdum purus iaculis. Proin maximus dolor tortor, sagittis efficitur nisl vestibulum vel. Vivamus id porttitor lacus. Sed mollis purus eget laoreet pretium. Morbi est ligula, convallis sit amet ultrices vel, sodales non sapien. Ut in cursus lacus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam laoreet imperdiet lectus, ut posuere magna tincidunt sit amet. Vivamus non est nec ipsum vehicula imperdiet. Ut varius auctor dolor nec rhoncus. Duis eu risus pretium, pulvinar eros id, mattis elit. Duis non erat rhoncus risus ultrices suscipit non in justo. Etiam nec dapibus nunc.<br/>Nam maximus nec tellus vel rutrum. Integer hendrerit accumsan lobortis. Sed tristique dui a quam venenatis euismod. Duis feugiat fringilla tristique. Vestibulum non efficitur libero. Duis tempor euismod tincidunt. Mauris pellentesque arcu tempor, laoreet nisi sed, congue urna. Fusce sed justo turpis. In at dolor ipsum. Mauris consectetur, diam sit amet aliquet tincidunt, neque libero ullamcorper felis, vitae consectetur enim massa a erat. Mauris luctus eget sapien quis elementum. Nunc eleifend lobortis felis non gravida.<br/>Maecenas in accumsan velit. Sed volutpat urna sed dolor bibendum aliquam. Duis eu vehicula quam. Duis facilisis sapien sed est tempus auctor ac sit amet sapien. Pellentesque et laoreet sapien. Aenean tincidunt, odio vel ultricies vulputate, velit tellus sagittis quam, at interdum enim tellus eu felis. Sed lacinia vehicula urna quis condimentum. Duis vehicula fermentum turpis. Fusce nibh quam, fermentum vitae volutpat ac, scelerisque at nibh. Sed ultrices mauris orci, quis venenatis tortor auctor eget. Nulla tempus nisi at ex porttitor luctus. Fusce non elit ut leo eleifend sollicitudin. Nullam ullamcorper, sem at tempor commodo, sapien risus molestie lectus, eu consequat dui elit vitae turpis.<br/>Ut quis volutpat felis, vitae euismod velit. Morbi fermentum fermentum justo at mattis. In venenatis lectus sit amet diam tincidunt euismod. Maecenas condimentum tincidunt pulvinar. Nam commodo ultricies purus, at maximus ante auctor a. Integer ut eleifend velit. Mauris vitae justo velit. Cras facilisis ante nec est tempus, sit amet cursus felis interdum. Etiam sollicitudin viverra nibh. Fusce fermentum est mi, quis hendrerit erat tempus in. Vivamus molestie mauris turpis, nec mattis dui iaculis vitae. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nullam mollis nulla a metus volutpat, at dapibus augue blandit. Nullam non risus vel tellus placerat laoreet. Aenean ac aliquam dui. Vivamus rutrum nibh quis neque hendrerit, efficitur gravida ligula consequat.<br/>Nullam ut fermentum sapien. Vivamus luctus commodo aliquet. Nunc tellus urna, tristique nec suscipit vel, commodo a nisi. Donec lobortis urna ut interdum laoreet. Donec congue nisi nec nibh euismod commodo. Donec blandit id sem eget ornare. Mauris non quam vel ex viverra elementum. Phasellus pharetra tempor tellus ut blandit. Aliquam lobortis luctus tincidunt. Curabitur erat risus, placerat non est et, cursus efficitur ex. Donec porttitor venenatis augue et volutpat. Quisque non nibh ac nulla convallis scelerisque. Pellentesque libero urna, efficitur eu tortor non, sollicitudin facilisis mauris. Morbi imperdiet laoreet congue.';
        $data = [
            ['id' => 1, 'name' => 'Materi Stoikometri', 'level' => 1, 'study' => 'IPA', 'header' => 'stoikometri.png', 'active' => 1, 'description' => $description],
            ['id' => 2, 'name' => 'Materi Aturan Sinus dan Cosinus', 'level' => 2, 'study' => 'Matematika', 'header' => 'logaritma.png', 'active' => 1, 'description' => $description],
            ['id' => 3, 'name' => 'Materi Puisi', 'level' => 3, 'study' => 'Bahasa Indonesia', 'header' => 'puisi.png', 'active' => 1, 'description' => $description],
            ['id' => 4, 'name' => 'Materi Aljabar', 'level' => 1, 'study' => 'Matematika', 'header' => 'aljabar.png', 'active' => 1, 'description' => $description],
            ['id' => 5, 'name' => 'Materi Narrative Text', 'level' => 2, 'study' => 'Bahasa Inggris', 'header' => 'narrative.png', 'active' => 1, 'description' => $description],
        ];
        foreach ($data as $d) {
            $course = new Course;
            $course->id = $d['id'];
            $course->name = $d['name'];
            $course->level = $d['level'];
            $course->study = $d['study'];
            $course->header = $d['header'];
            $course->active = $d['active'];
            $course->description = $d['description'];
            $course->save();
        }
    }
}
