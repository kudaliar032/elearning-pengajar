<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = [
            ['Aditya Rahman', 'adit', 'adit@mail.com', 'adit.png', 0, 'adit'],
            ['Mega Intanadias Calista', 'mega', 'mega@mail.com', 'mega.png', 1, 'mega'],
            ['Admin', 'admin', 'admin@mail.com', 'admin.png', 0, 'admin'],
            ['User Siswa', 'siswa', 'siswa@mail.com', 'siswa.png', 2, 'siswa']
        ];
        foreach ($admin as $a) {
            $user = new User;
            $user->name = $a[0];
            $user->username = $a[1];
            $user->email = $a[2];
            $user->photo = $a[3];
            $user->role = $a[4];
            $user->password = Hash::make($a[5]);
            $user->save();
        }
    }
}
