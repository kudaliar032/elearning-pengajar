<?php

namespace App\Http\Controllers;

use App\Course;
use App\Exam;
use Illuminate\Http\Request;

class ExamsController extends Controller
{
    public function index() {
        $data['courses'] = Course::all();

        return view('exams.index', $data);
    }

    public function questions($course_id) {
        $data['questions'] = Exam::where('course_id', '=', $course_id)->orderBy('created_at')->get();
        $data['course_id'] = $course_id;
        return view('exams.questions', $data);
    }

    public function questionAdd($course_id) {
        $data['course_id'] = $course_id;
        return view('exams.question.add', $data);
    }

    public function questionAddSave(Request $request) {
        $question = new Exam;
        $question->course_id = $request->course_id;
        $question->question = $request->question;
        $question->a = $request->a;
        $question->b = $request->b;
        $question->c = $request->c;
        $question->d = $request->d;
        $question->e = $request->e;
        $question->answer = $request->answer;
        $question->save();

        return redirect()->route('courses.exam', $request->course_id);
    }

    public function questionEdit($id) {
        $data['question'] = Exam::find($id);
        return view('exams.question.edit', $data);
    }

    public function questionEditSave(Request $request) {
        $question = Exam::find($request->id);
        $question->question = $request->question;
        $question->a = $request->a;
        $question->b = $request->b;
        $question->c = $request->c;
        $question->d = $request->d;
        $question->e = $request->e;
        $question->answer = $request->answer;
        $question->save();

        return redirect()->route('courses.exam', $request->course_id);
    }

    public function questionDelete($course_id, $id) {
        $question = Exam::find($id)->delete();

        return redirect()->route('courses.exam', $course_id);
    }
}
