<?php

namespace App\Http\Controllers\Api;

use App\Course;
use App\Exam;
use App\Http\Controllers\Controller;
use App\Module;
use App\User;
use App\UsersCourse;
use App\UsersModule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CourseController extends Controller
{
    public function index(Request $request) {
        return Course::all('id', 'name', 'level', 'study', 'header');
    }

    public function courseDetail($id, Request $request) {
        $user_id = $request->user()->id;
        $modules = Module::where('course_id', $id)->get(['id', 'title']);
        $detail = Course::find($id);
        $detail->modules_total = count($modules);
        $detail->exams_total = Exam::where('course_id', $id)->count();
        $detail->modules_done = UsersModule::where('course_id', $id)->where('user_id', $request->user()->id)->count();

        foreach ($modules as $m) {
            if (UsersModule::where('user_id', $user_id)->where('module_id', $m->id)->count() > 0) {
                $m->done = true;
            } else {
                $m->done = false;
            }
        }

        $last_open = UsersModule::where('course_id', $id)
            ->where('user_id', $request->user()->id)
            ->orderBy('created_at', 'desc');
        $detail->last_open = $last_open->count() > 0 ? $last_open->first()->id : null;

        return [
            'detail' => $detail,
            'modules' => $modules,
        ];
    }

    public function notMyCourse(Request $request) {
        $study = strtolower($request->get('study'));
        $like = '%';

        $my_courses = DB::table('users_courses')->where('user_id', $request->user()->id);
        if (in_array($study, array('matematika', 'ipa', 'indonesia', 'inggris'))) {
            $like = '%'.$study;
        }

        return DB::table('courses')
            ->select('courses.id AS id', 'name', 'level', 'study', 'header')
            ->addSelect(DB::raw('(SELECT COUNT(course_id) FROM modules WHERE course_id = courses.id) AS total_modules'))
            ->leftJoinSub($my_courses,'my_courses', 'courses.id', '=', 'my_courses.course_id')
            ->whereNull('my_courses.course_id')
            ->orderBy('my_courses.created_at')
            ->where('study', 'LIKE', $like)
            ->get();
    }
}
