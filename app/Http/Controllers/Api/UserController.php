<?php

namespace App\Http\Controllers\Api;

use App\Course;
use App\Http\Controllers\Controller;
use App\User;
use App\UsersCourse;
use App\UsersDetail;
use App\UsersExam;
use App\UsersModule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Exception;

class UserController extends Controller
{
    public function takeCourse($course_id, Request $request) {
        $users_courses = new UsersCourse;
        $users_courses->user_id = $request->user()->id;
        $users_courses->course_id = $course_id;
        if (UsersCourse::where('user_id', $request->user()->id)->where('course_id', $course_id)->count() > 0)
            return response()->json(['status' => 'exist']);
        if ($users_courses->save()) {
            return response()->json(['status' => 'success']);
        } else {
            return response()->json(['status' => 'fail']);
        }
    }

    public function courseList(Request $request) {
        $study = strtolower($request->get('study'));
        $like = '%';
        $my_courses = DB::table('users_courses')->where('user_id', $request->user()->id);
        if (in_array($study, array('matematika', 'ipa', 'indonesia', 'inggris'))) {
            $like = '%'.$study;
        }
        return DB::table('courses')
            ->select('courses.id AS id', 'name', 'level', 'study', 'header')
            ->addSelect(DB::raw('(SELECT COUNT(module_id) FROM users_modules WHERE course_id = courses.id AND user_id = my_courses.user_id) AS total_opened_modules'))
            ->addSelect(DB::raw('(SELECT COUNT(course_id) FROM modules WHERE course_id = courses.id) AS total_modules'))
            ->joinSub($my_courses, 'my_courses', 'courses.id', '=', 'my_courses.course_id')
            ->orderBy('my_courses.created_at', 'DESC')
            ->where('study', 'LIKE', $like)
            ->get();
    }

    public function detail() {
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }

        $user_id = $user->id;
        $detail = User::find($user_id)->detail;

        return response()->json(compact(['user', 'detail']));
    }

    public function openModule($course_id, $module_id, Request $request) {
        $users_module = new UsersModule;
        $users_module->user_id = $request->user()->id;
        $users_module->module_id = $module_id;
        $users_module->course_id = $course_id;
        if (UsersModule::where('user_id', $request->user()->id)->where('module_id', $module_id)->count() > 0)
            return response()->json(['status' => 'opened']);
        if ($users_module->save()) {
            return response()->json(['status' => 'success']);
        } else {
            return response()->json(['status' => 'fail']);
        }
    }

    public function courseScore($course_id) {
        $data = UsersExam::where('course_id', $course_id)->get();
        foreach ($data as $d) {
            $d['answers_recap'] = json_decode($d['answers_recap']);
        }
        return $data;
    }

    public function addDetail(Request $request) {
        try {
            $detail = new UsersDetail;
            $detail->user_id = $request->user()->id;
            $detail->tempat_lahir = $request->tempat_lahir;
            $detail->tanggal_lahir = $request->tanggal_lahir;
            $detail->sekolah = $request->sekolah;
            $detail->kelas = $request->kelas;
            $detail->alamat = $request->alamat;
            $detail->kelurahan = $request->kelurahan;
            $detail->kecamatan = $request->kecamatan;
            $detail->kabupaten = $request->kabupaten;
            $detail->provinsi = $request->provinsi;

            $detail->save();
            return ['status' => 'success'];
        } catch (Exception $e) {
            return ['status' => 'fail'];
        }
    }
}
