<?php

namespace App\Http\Controllers\Api;

use App\Exam;
use App\Http\Controllers\Controller;
use App\UsersExam;
use Illuminate\Http\Request;

class ExamController extends Controller
{
    public function getQuestions($course_id)
    {
        $questions = Exam::select('id', 'question', 'a', 'b', 'c', 'd', 'e')
            ->where('course_id', $course_id)
            ->orderBy('id')
            ->get();

        foreach ($questions as $q) {
            $q->answer = null;
        }

        return $questions;
    }

    public function answerCheck(Request $request)
    {
        $data['total_true'] = 0;
        $data['total_questions'] = count($request->input('answers'));

        # answer check
        foreach ($request->input('answers') as $k => $a) {
            $isTrue = Exam::where('id', $a['id'])->where('answer', $a['answer'])->count();
            if ($isTrue > 0) {
                $answer = true;
                $data['total_true'] += 1;
            } else {
                $answer = false;
            }
            $result[$k] = ['id' => $a['id'], 'answer' => $answer];
        }
        $data['answer'] = $result;

        # save to database
        $users_exams = new UsersExam;
        $users_exams->user_id = $request->user()->id;
        $users_exams->course_id = $request->input('course_id');
        $users_exams->total_true = $data['total_true'];
        $users_exams->total_questions = $data['total_questions'];
        $users_exams->answers_recap = json_encode($data['answer']);

        if ($users_exams->save()) {
            return ['status' => 'success', 'data' => $data];
        } else {
            return ['status' => 'fail'];
        }
    }
}
