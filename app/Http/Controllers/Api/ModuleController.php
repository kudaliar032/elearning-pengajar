<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Module;
use App\User;
use App\UsersModule;
use Illuminate\Http\Request;

class ModuleController extends Controller
{
    public function getModuleList($course_id, Request $request) {
        $user_id = $request->user()->id;
        $modules = Module::where('course_id', $course_id)->orderBy('id')->get(['id', 'course_id', 'title']);
        $last_open_query = UsersModule::where('course_id', $course_id)
            ->where('user_id', $user_id)
            ->orderBy('created_at', 'desc');

        foreach ($modules as $m) {
            if (UsersModule::where('user_id', $user_id)->where('module_id', $m->id)->count() > 0) {
                $m->done = true;
            } else {
                $m->done = false;
            }
        }

        if ($last_open_query->count() > 0) {
            $last_open = $last_open_query->first()->id;
        } else {
            $last_open = null;
        }

        return [
            'last_open' => $last_open,
            'modules' => $modules
        ];
    }

    public function getModule($course_id, $module_id) {
        $detail = Module::where('id', $module_id)->where('course_id', $course_id)->first();

        return $detail;
    }
}
