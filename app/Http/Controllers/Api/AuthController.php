<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Exception;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('username', 'password');

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        return response()->json(compact('token'));
    }

    public function register(Request $request)
    {
        try {
            $user = User::create([
                'name' => $request->name,
                'username' => $request->username,
                'email' => $request->email,
                'role' => 2,
                'password' => Hash::make($request->password)
            ]);
            $token = JWTAuth::fromUser($user);
            return response()->json(compact('token'));
        } catch (Exception $e) {
            return response()->json(['error' => 'register invalid'], 500);
        }
    }

    public function verify(Request $request) {
        $token = explode(' ', $request->header('Authorization'));
        return response()->json(['token' => $token[1]]);
    }
}
