<?php

namespace App\Http\Controllers;

use App\Exam;
use App\Module;
use Illuminate\Http\Request;
use App\Course;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CoursesController extends Controller
{
    public function __construct()
    {
        $this->_studies = ['Matematika', 'Bahasa Inggris', 'Bahasa Indonesia', 'IPA'];
    }

    public function index() {
        $data['courses'] = Course::all();
        $data['totalModules'] = DB::table('modules')
            ->selectRaw('course_id, COUNT(course_id) as totalModules')
            ->groupBy('course_id')
            ->get();

        return view('courses.index', $data);
    }

    public function add() {
        $data['studies'] = $this->_studies;
        return view('courses.add', $data);
    }

    public function addSave(Request $request) {
        $photo = $request->header;
        $fileName = 'h-'.time().'.png';

        if ($photo->storeAs('header_photos/', $fileName)) {
            $course = new Course;
            $course->name = $request->name;
            $course->level = $request->level;
            $course->study = $request->study;
            $course->header = $fileName;
            $course->active = 1;
            $course->description = $request->description;
            $course->save();
        }

        return redirect()->route('courses.list');
    }

    public function edit($id) {
        $data['studies'] = $this->_studies;
        $data['course'] = Course::find($id);
        return view('courses.edit', $data);
    }

    public function editSave(Request $request) {
        $course = Course::find($request->id);
        $course->name = $request->name;
        $course->level = $request->level;
        $course->study = $request->study;
        $course->description = $request->description;
        $course->save();

        return redirect()->route('courses.list');
    }

    public function editHeader($id) {
        $data['course_id'] = $id;
        $data['header'] = Course::select('header')->find($id)->header;

        return view('courses.header', $data);
    }

    public function editHeaderSave(Request $request) {
        $course = Course::find($request->id);
        $photo = $request->header;
        $photoName = $course->header;

        if (!$course->header) {
            if ($photo->storeAs('header_photos', $photoName)) {
                $course->header = $photoName;
                $course->save();
            }
        } else {
            if (Storage::delete('header_photos/'.$course->header) && $photo->storeAs('header_photos', $photoName)) {
                $course->header = $photoName;
                $course->save();
            }
        }

        return redirect()->route('courses.list');
    }

    public function delete($id) {
        $course = Course::find($id);
        $modules = Module::where('course_id', $id);
        $exams = Exam::where('course_id', $id);

        $filePath = 'header_photos/'.$course->header;
        if ($course->header == null || Storage::delete($filePath)) {
            $course->delete();
            $modules->delete();
            $exams->delete();
        }

        return redirect()->route('courses.list');
    }

}
