<?php

namespace App\Http\Controllers;

use App\Course;
use App\Module;
use Illuminate\Http\Request;

class ModulesController extends Controller
{
    public function index() {
        $data['courses'] = Course::all();
        return view('modules.index', $data);
    }

    public function modulesList($course_id) {
        $data['modules'] = Module::where('course_id', '=', $course_id)->orderBy('created_at')->get();
        $data['course_id'] = $course_id;
        return view('modules.list', $data);
    }

    public function add($course_id) {
        $data['course_id'] = $course_id;
        return view('modules.add', $data);
    }

    public function addSave(Request $request) {
        $module = new Module;
        $module->title = $request->title;
        $module->course_id = $request->course_id;
        $module->module_content = $request->module_content;
        $module->save();

        return redirect()->route('courses.modules.list', $request->course_id);
    }

    public function edit($id) {
        $data['module'] = Module::find($id);
        return view('modules.edit', $data);
    }

    public function editSave(Request $request) {
        $module = Module::find($request->id);
        $module->title = $request->title;
        $module->course_id = $request->course_id;
        $module->module_content = $request->module_content;
        $module->save();

        return redirect()->route('courses.modules.list', $request->course_id);
    }

    public function delete($course_id, $id) {
        $module = Module::find($id)->delete();
        return redirect()->route('courses.modules.list', $course_id);
    }
}
