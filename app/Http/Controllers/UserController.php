<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function list() {
        $data['users'] = User::all();
        return view('user.list', $data);
    }

    public function add() {
        return view('user.add');
    }

    public function addSave(Request $request) {
        $photo = $request->profile;
        $photoName = $request->username.'.png';

        if ($photo->storeAs('photo_profile', $photoName)) {
            $user = new User;
            $user->name = $request->name;
            $user->username = $request->username;
            $user->email = $request->email;
            $user->photo = $photoName;
            $user->role = $request->role;
            $user->password = Hash::make($request->password);
            $user->save();
        }

        return redirect()->route('user.list');
    }

    public function edit($id) {
        $data['user'] = User::find($id);
        return view('user.edit', $data);
    }

    public function editSave(Request $request) {
        $user = User::find($request->id);
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->save();

        return redirect()->route('user.list');
    }

    public function editPhoto($id) {
        $data['user_id'] = $id;
        $data['profile'] = User::find($id)->photo;

        return view('user.profile', $data);
    }

    public function editPhotoSave(Request $request) {
        $user = User::find($request->id);
        $photo = $request->profile;
        $photoName = $user->username.'.png';

        if (!$user->photo) {
            if ($photo->storeAs('photo_profile', $photoName)) {
                $user->photo = $photoName;
                $user->save();
            }
        } else {
            if (Storage::delete('photo_profile/'.$user->photo) && $photo->storeAs('photo_profile', $photoName)) {
                $user->photo = $photoName;
                $user->save();
            }
        }

        return redirect()->route('user.list');
    }

    public function delete($id) {
        $user = User::find($id);
        $filePath = 'photo_profile/'.$user->photo;
        if ($user->photo == null || Storage::delete($filePath)) {
            $user->delete();
        }

        return redirect()->route('user.list');
    }
}
