<?php

namespace App\Http\Controllers;

use App\Course;
use App\Exam;
use App\Module;
use App\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index() {
        $data['usersTotal'] = User::all()->count();
        $data['coursesTotal'] = Course::all()->count();
        $data['modulesTotal'] = Module::all()->count();
        $data['examsTotal'] = Exam::all()->count();
        return view('dashboard.index', $data);
    }
}
