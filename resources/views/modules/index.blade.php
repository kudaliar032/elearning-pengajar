@extends('template.app')

@section('title', 'Courses list')

@section('content')
    <div class="row">
        <section class="col">
            <div class="card">
                <div class="card-body">
                    <table id="modules-table" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Course name</th>
                            <th>Total modules</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($courses as $course)
                            <tr>
                                <td>
                                    <a href="{{route('courses.modules.list', $course['id'])}}">
                                        {{$course['name']}}
                                    </a>
                                </td>
                                <td>{{\App\Module::where('course_id', $course->id)->count()}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- /.card-body -->
            </div>
        </section>
    </div>
@stop

@section('javascript')
    <script>
        $(function () {
            $("#modules-table").DataTable();
        });
    </script>
@stop
