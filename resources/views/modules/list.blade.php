@extends('template.app')

@section('title', 'Modules List')

@section('content')
    <div class="row">
        <section class="col">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-book mr-1"></i>
                    </h3>
                    <div class="card-tools">
                        <ul class="nav nav-pills ml-auto">
                            <li class="nav-item">
                                <a href="{{route('courses.module.add', $course_id)}}">
                                    <button class="btn btn-success">
                                        <i class="fas fa-plus"></i>
                                        Add
                                    </button>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div><!-- /.card-header -->
                <div class="card-body">
                    <table id="modules-table" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Module title</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($modules as $module)
                            <tr>
                                <td>{{$module['title']}}</td>
                                <td>
                                    <a href="{{route('courses.module.delete', [$course_id, $module['id']])}}">
                                        <button class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                    </a>
                                    <a href="{{route('courses.module.edit', $module['id'])}}">
                                        <button class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></button>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- /.card-body -->
            </div>
        </section>
    </div>
@stop

@section('javascript')
    <script>
        $(function () {
            $("#modules-table").DataTable();
        });
    </script>
@stop
