@extends('template.app')

@section('title', 'Add Module')

@section('content')
    <div class="row">
        <section class="col">
            <div class="card">
                <form method="post" action="{{route('courses.module.add.save')}}">
                    @csrf
                    <input value="{{$course_id}}" name="course_id" hidden>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="title">Module Title</label>
                            <input type="text" class="form-control" id="title" placeholder="Enter module title" name="title" required>
                        </div>
                        <div class="form-group">
                            <label>Content</label>
                            <textarea required id="module_content" name="module_content"></textarea>
                        </div>
                    </div>

                    <div class="card-footer">
                        <a href="{{route('courses.modules.list', $course_id)}}">
                            <button type="button" class="btn btn-danger">Cancel</button>
                        </a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </section>
    </div>
@stop

@section('javascript')
    <script>
        CKEDITOR.replace('module_content', {
            height: 800
        });
    </script>
@stop
