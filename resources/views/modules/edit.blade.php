@extends('template.app')

@section('title', 'Edit Module')

@section('content')
    <div class="row">
        <section class="col">
            <div class="card">
                <form method="post" action="{{route('courses.module.edit.save')}}">
                    @csrf
                    <input name="id" value="{{$module->id}}" hidden/>
                    <input name="course_id" value="{{$module->course_id}}" hidden/>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="title">Module Title</label>
                            <input type="text" class="form-control" id="title" value="{{$module->title}}" name="title" required>
                        </div>
                        <div class="form-group">
                            <label>Content</label>
                            <textarea required id="module_content" name="module_content">{{$module->module_content}}</textarea>
                        </div>
                    </div>

                    <div class="card-footer">
                        <a href="{{route('courses.modules.list', $module->course_id)}}">
                            <button type="button" class="btn btn-danger">Cancel</button>
                        </a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </section>
    </div>
@stop

@section('javascript')
    <script>
        CKEDITOR.replace('module_content', {
            height: 800
        });
    </script>
@stop
