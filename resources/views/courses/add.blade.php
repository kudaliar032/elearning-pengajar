@extends('template.app')

@section('title', 'Add Course')

@section('head')
    <style>
        #headerCrop {
            display: none;
            max-height: 300px;
            height: 300px;
        }
    </style>
@stop

@section('content')
    <div class="row">
        <section class="col">
            <div class="card">
                <form id="addForm" method="post">
                    {{csrf_field()}}
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">Course name</label>
                            <input type="text" class="form-control" id="name"
                                   placeholder="Enter course name" name="name" required>
                        </div>
                        <div class="form-group">
                            <label>Course level</label>
                            <select class="form-control" name="level" required>
                                <option value="" disabled selected>-- Select level --</option>
                                <option value="1">Kelas 10</option>
                                <option value="2">Kelas 11</option>
                                <option value="3">Kelas 12</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Study name</label>
                            <select class="form-control" name="study" required>
                                <option value="" disabled selected>-- Select study name --</option>
                                @foreach($studies as $s)
                                    <option value="{{$s}}">{{$s}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-6">
                            <img src="{{asset('/img/default-header.png')}}" id="headerCrop" />
                        </div>
                        <div class="form-group mt-2">
                            <label for="header">Header photo</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="header" required>
                                    <label class="custom-file-label" for="header">Choose header</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea required id="description" name="description"></textarea>
                        </div>
                    </div>

                    <div class="card-footer">
                        <a href="{{route('courses.list')}}">
                            <button type="button" class="btn btn-danger">Cancel</button>
                        </a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </section>
    </div>
@stop

@section('javascript')
    <script>
        $(function () {
            /*
            Image croppper
             */
            const headerCrop = document.getElementById('headerCrop');
            let cropper;

            const cropImage = (input) => {
                if (input.files && input.files[0]) {
                    const reader = new FileReader();
                    reader.onload = e => {
                        if (cropper) {
                            cropper.replace(e.target.result);
                        } else {
                            headerCrop.style.display = "block";
                            headerCrop.setAttribute('src', e.target.result);
                            cropper = new Cropper(headerCrop, {
                                aspectRatio: 2 / 1,
                                viewMode: 2,
                                zoomOnWheel: false
                            });
                        }

                    };
                    reader.readAsDataURL(input.files[0]);
                }
            };

            $("#header").change(function () {
                cropImage(this);
            });

            /*
            Send form
             */
            $('#addForm').submit(e => {
                e.preventDefault();
                cropper.getCroppedCanvas({height: 500, width: 1000}).toBlob(blob => {
                    const formData = new FormData();
                    formData.append('name', $('[name=name]').val());
                    formData.append('level', $('[name=level]').val());
                    formData.append('study', $('[name=study]').val());
                    formData.append('header', blob);
                    formData.append('description', $('[name=description]').val());

                    axios({
                        method: 'post',
                        url: "{{route('courses.add.save')}}",
                        data: formData,
                        headers: {
                            'Content-Type': 'multipart/form-data',
                        }
                    }).then(res => {
                        window.location.replace("{{route('courses.list')}}")
                    }).catch(err => {
                        alert('Add course error');
                        {{--window.location.replace("{{route('courses.list')}}")--}}
                    });
                });
            });
        });

        /*
        CKEditor
         */
        CKEDITOR.replace('description');
    </script>
@stop
