@extends('template.app')

@section('title', 'Courses')

@section('content')
    <div class="row">
        <section class="col">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-book mr-1"></i>
                    </h3>
                    <div class="card-tools">
                        <ul class="nav nav-pills ml-auto">
                            <li class="nav-item">
                                <a href="{{route('courses.add')}}">
                                    <button class="btn btn-success">
                                        <i class="fas fa-plus"></i>
                                        Add
                                    </button>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="card-body">
                    <table id="courses-table" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Course name</th>
                            <th>Course level</th>
                            <th>Header photo</th>
                            <th>Total modules</th>
                            <th>Total questions</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($courses as $course)
                            <tr>
                                <td>{{$course['name']}}</td>
                                <td>{{$course['level']}}</td>
                                <td><a target="_blank" href="{{asset('header_photos/'.$course->header)}}">{{$course->header}}</a></td>
                                <td>
                                    <a href="{{route('courses.modules.list', $course->id)}}">
                                        {{\App\Module::where('course_id', $course->id)->count()}}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{route('courses.exam', $course->id)}}">
                                        {{\App\Exam::where('course_id', $course->id)->count()}}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{route('courses.edit', $course['id'])}}">
                                        <button class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></button>
                                    </a>
                                    <a href="{{route('courses.edit.header', $course['id'])}}">
                                        <button class="btn btn-primary btn-sm"><i class="fas fa-image"></i></button>
                                    </a>
                                    <a href="{{route('courses.delete', $course['id'])}}">
                                        <button class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- /.card-body -->
            </div>
        </section>
    </div>
@stop

@section('javascript')
    <script>
        $(function () {
            $("#courses-table").DataTable();
        });
    </script>
@stop
