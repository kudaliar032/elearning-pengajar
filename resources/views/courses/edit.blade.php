@extends('template.app')

@section('title', 'Edit Course')

@section('content')
    <div class="row">
        <section class="col">
            <div class="card">
                <form method="post" action="{{route('courses.edit.save')}}">
                    {{csrf_field()}}
                    <input type="text" name="id" value="{{$course->id}}" hidden/>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">Course Name</label>
                            <input type="text" class="form-control" id="name" value="{{$course['name']}}" name="name" required>
                        </div>
                        <div class="form-group">
                            <label>Course Level</label>
                            <select class="form-control" name="level" required>
                                <option value="" disabled selected>-- Select level --</option>
                                <option value="1" {{$course['level'] == 1 ? 'selected' : ''}}>Kelas 10</option>
                                <option value="2" {{$course['level'] == 2 ? 'selected' : ''}}>Kelas 11</option>
                                <option value="3" {{$course['level'] == 3 ? 'selected' : ''}}>Kelas 12</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Study name</label>
                            <select class="form-control" name="study" required>
                                <option value="" disabled selected>-- Select study name --</option>
                                @foreach($studies as $s)
                                    <option value="{{$s}}" {{$course->study == $s ? "selected" : null}}>{{$s}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea required id="description" name="description">{{$course['description']}}</textarea>
                        </div>
                    </div>

                    <div class="card-footer">
                        <a href="{{route('courses.list')}}">
                            <button type="button" class="btn btn-danger">Cancel</button>
                        </a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </section>
    </div>
@stop

@section('javascript')
    <script>
        CKEDITOR.replace('description');
    </script>
@stop
