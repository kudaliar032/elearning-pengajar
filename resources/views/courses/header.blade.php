@extends('template.app')

@section('head')
    <style>
        #headerCrop {
            display: block;
            max-height: 300px;
            height: 300px;
        }
    </style>
@stop

@section('title', 'Edit Course Header Photo')

@section('content')
    <div class="row">
        <section class="col">
            <div class="card">
                <form id="editForm" method="post">
                    @csrf
                    <input name="id" value="{{$course_id}}" type="text" hidden/>
                    <div class="card-body">
                        <div class="col-6">
                            <img src="{{$header == null ? asset('/img/default-header.png') : asset('/header_photos/'.$header)}}" id="headerCrop">
                        </div>
                        <div class="form-group mt-2">
                            <label for="header">Header photo</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="header" name="header">
                                    <label class="custom-file-label" for="header">Choose header photo</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <a href="{{route('courses.list')}}">
                            <button type="button" class="btn btn-danger">Cancel</button>
                        </a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </section>
    </div>
@stop

@section('javascript')
    <script>
        $(function () {
            /*
            Image croppper
             */
            const headerCrop = document.getElementById('headerCrop');
            const cropper = new Cropper(headerCrop, {
                aspectRatio: 2 / 1,
                viewMode: 2,
                zoomOnWheel: false,
                autoCropArea: 1
            });

            const cropImage = input => {
                if (input.files && input.files[0]) {
                    const reader = new FileReader();
                    reader.onload = e => {
                        cropper.replace(e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            };

            $("#header").change(function () {
                cropImage(this);
            });

            $('#editForm').submit(e => {
                e.preventDefault();
                cropper.getCroppedCanvas({height: 500, width: 1000}).toBlob(blob => {
                    const formData = new FormData();
                    formData.set('id', $("[name=id]").val());
                    formData.append('header', blob);

                    axios({
                        method: 'post',
                        url: "{{route('courses.edit.header.save')}}",
                        data: formData,
                        headers: {
                            'Content-Type': 'multipart/form-data',
                        }
                    }).then(res => {
                        window.location.replace("{{route('courses.list')}}")
                    }).catch(err => {
                        alert('Edit course header error');
                        {{--window.location.replace("{{route('courses.list')}}")--}}
                    });
                });
            });
        })
    </script>
@stop
