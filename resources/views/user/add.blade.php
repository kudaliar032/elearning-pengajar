@extends('template.app')

@section('title', 'Add Account')

@section('head')
    <style>
        #photoProfile {
            display: none;
            max-height: 300px;
            height: 300px;
        }
    </style>
@stop

@section('content')
    <div class="row">
        <section class="col">
            <div class="card">
                <form id="addForm">
                    {{csrf_field()}}
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">Full name</label>
                            <input type="text" class="form-control" id="name" placeholder="Enter full name" name="name"
                                   required>
                        </div>
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" class="form-control" id="username" placeholder="Enter username"
                                   name="username" required>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" placeholder="Enter email address"
                                   name="email" required>
                        </div>
                        <div class="form-group">
                            <label>Role</label>
                            <select class="form-control" name="role" required>
                                <option value="" disabled selected>-- Select role --</option>
                                <option value="0">Admin</option>
                                <option value="1">Pengajar</option>
                                <option value="2">Siswa</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" placeholder="Enter password"
                                   name="password" required>
                        </div>
                        <div class="col-6">
                            <img src="{{asset('/img/default-header.png')}}" id="photoProfile" />
                        </div>
                        <div class="form-group mt-2">
                            <label for="profile">Photo profile</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="profile" name="profile" required>
                                    <label class="custom-file-label" for="profile">Choose photo profile</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <a href="{{route('user.list')}}">
                            <button type="button" class="btn btn-danger">Cancel</button>
                        </a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </section>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(() => {
            /*
            Image cropper
             */
            const photoProfile = document.getElementById('photoProfile');
            let cropper;

            const cropImage = (input) => {
                if (input.files && input.files[0]) {
                    const reader = new FileReader();
                    reader.onload = e => {
                        if (cropper) {
                            cropper.replace(e.target.result);
                        } else {
                            photoProfile.style.display = "block";
                            photoProfile.setAttribute('src', e.target.result);
                            cropper = new Cropper(photoProfile, {
                                aspectRatio: 1,
                                viewMode: 2,
                                zoomOnWheel: false,
                                autoCropArea: 1
                            });
                        }

                    };
                    reader.readAsDataURL(input.files[0]);
                }
            };

            $("#profile").change(function () {
                cropImage(this);
            });

            /*
            Form send
             */
            $('#addForm').submit(e => {
                e.preventDefault();
                cropper.getCroppedCanvas({height: 500, width: 500}).toBlob(blob => {
                    const formData = new FormData();
                    formData.set('name', $('[name=name]').val());
                    formData.set('username', $('[name=username]').val());
                    formData.set('email', $('[name=email]').val());
                    formData.append('profile', blob);
                    formData.set('role', $('[name=role]').val());

                    axios({
                        method: 'post',
                        url: "{{route('user.add.save')}}",
                        data: formData,
                        headers: {
                            'Content-Type': 'multipart/form-data',
                        }
                    }).then(res => {
                        window.location.replace("{{route('user.list')}}")
                    }).catch(err => {
                        alert('Add user error');
                        {{--window.location.replace("{{route('user.list')}}")--}}
                    });
                });
            });
        });
    </script>
@stop
