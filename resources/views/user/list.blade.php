@extends('template.app')

@section('title', 'Users')

@section('content')
    <div class="row">
        <section class="col">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-user mr-1"></i>
                    </h3>
                    <div class="card-tools">
                        <ul class="nav nav-pills ml-auto">
                            <li class="nav-item">
                                <a href="{{route('user.add')}}">
                                    <button class="btn btn-success">
                                        <i class="fas fa-plus"></i>
                                        Add
                                    </button>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div><!-- /.card-header -->
                <div class="card-body">
                    <table id="users-table" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Role</th>
                            <th>Photo profile</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $u)
                            <tr>
                                <td>{{$u['name']}}</td>
                                <td>{{$u['username']}}</td>
                                <td>{{$u['role']}}</td>
                                <td><a href="{{asset('photo_profile/'.$u['photo'])}}" target="_blank">{{$u['photo']}}</a></td>
                                <td>
                                    <a href="{{route('user.edit', $u['id'])}}">
                                        <button class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></button>
                                    </a>
                                    <a href="{{route('user.edit.photo', $u['id'])}}">
                                        <button class="btn btn-primary btn-sm"><i class="fas fa-image"></i></button>
                                    </a>
                                    <a href="{{route('user.delete', $u['id'])}}">
                                        <button class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
@stop

@section('javascript')
    <script>
        $(function () {
            $("#users-table").DataTable();
        });
    </script>
@stop
