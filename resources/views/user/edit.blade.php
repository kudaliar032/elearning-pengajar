@extends('template.app')

@section('title', 'Edit Account')

@section('content')
    <div class="row">
        <section class="col">
            <div class="card">
                <form method="post" action="{{route('user.edit.save')}}">
                    {{csrf_field()}}
                    <input type="text" value="{{$user['id']}}" name="id" hidden>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">Full name</label>
                            <input type="text" class="form-control" id="name" value="{{$user['name']}}" name="name" required>
                        </div>
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" class="form-control" id="username" value="{{$user['username']}}" name="username" required>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" value="{{$user['email']}}" name="email" required>
                        </div>
                        <div class="form-group">
                            <label>Role</label>
                            <select class="form-control" name="role" required>
                                <option value="" disabled>-- Select role --</option>
                                <option value="0" {{$user['role'] == 0 ? 'selected' : ''}}>Admin</option>
                                <option value="1" {{$user['role'] == 1 ? 'selected' : ''}}>Pengajar</option>
                                <option value="2" {{$user['role'] == 2 ? 'selected' : ''}}>Siswa</option>
                            </select>
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <a href="{{route('user.list')}}">
                            <button type="button" class="btn btn-danger">Cancel</button>
                        </a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </section>
    </div>
@stop
