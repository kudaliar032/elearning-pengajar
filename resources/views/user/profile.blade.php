@extends('template.app')

@section('title', 'Edit Photo Profile')

@section('head')
    <style>
        #photoProfile {
            display: block;
            max-height: 300px;
            height: 300px;
        }
    </style>
@stop

@section('content')
    <div class="row">
        <section class="col">
            <div class="card">

                <form id="editForm">
                    @csrf
                    <input name="id" value="{{$user_id}}" hidden/>
                    <div class="card-body">
                        <div class="col-6">
                            <img src="{{$profile == null ? asset('/img/default-profile.png') : asset('/photo_profile/'.$profile)}}" id="photoProfile">
                        </div>
                        <div class="form-group mt-2">
                            <label for="photo">Photo profile</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="profile" name="profile">
                                    <label class="custom-file-label" for="photo">Choose photo profile</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <a href="{{route('user.list')}}">
                            <button type="button" class="btn btn-danger">Cancel</button>
                        </a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </section>
    </div>
@stop

@section('javascript')
    <script>
        $(function () {
            /*
            Image croppper
             */
            const photoProfile = document.getElementById('photoProfile');
            const cropper = new Cropper(photoProfile, {
                aspectRatio: 1,
                viewMode: 2,
                zoomOnWheel: false,
                autoCropArea: 1
            });

            const cropImage = input => {
                if (input.files && input.files[0]) {
                    const reader = new FileReader();
                    reader.onload = e => {
                        cropper.replace(e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            };

            $("#profile").change(function () {
                cropImage(this);
            });

            $('#editForm').submit(e => {
                e.preventDefault();
                cropper.getCroppedCanvas({height: 500, width: 500}).toBlob(blob => {
                    const formData = new FormData();
                    formData.set('id', $("[name=id]").val());
                    formData.append('profile', blob);

                    axios({
                        method: 'post',
                        url: "{{route('user.edit.photo.save')}}",
                        data: formData,
                        headers: {
                            'Content-Type': 'multipart/form-data',
                        }
                    }).then(res => {
                        window.location.replace("{{route('user.list')}}")
                    }).catch(err => {
                        alert('Edit photo profile error');
                        {{--window.location.replace("{{route('user.list')}}")--}}
                    });
                });
            });
        })
    </script>
@stop
