@extends('template.app')

@section('title', 'Edit Question')

@section('content')
    <div class="row">
        <section class="col">
            <div class="card">
                <form method="post" action="{{route('courses.exam.question.edit.save')}}">
                    {{csrf_field()}}
                    <input value="{{$question->id}}" name="id" hidden>
                    <input value="{{$question->course_id}}" name="course_id" hidden>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Question</label>
                            <textarea required id="question" name="question">{{$question->question}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="a">A Choice</label>
                            <input type="text" class="form-control" id="a" value="{{$question->a}}" name="a" required>
                        </div>
                        <div class="form-group">
                            <label for="b">B Choice</label>
                            <input type="text" class="form-control" id="b" value="{{$question->b}}" name="b" required>
                        </div>
                        <div class="form-group">
                            <label for="c">C Choice</label>
                            <input type="text" class="form-control" id="c" value="{{$question->c}}" name="c" required>
                        </div>
                        <div class="form-group">
                            <label for="d">D Choice</label>
                            <input type="text" class="form-control" id="d" value="{{$question->d}}" name="d" required>
                        </div>
                        <div class="form-group">
                            <label for="e">E Choice</label>
                            <input type="text" class="form-control" id="e" value="{{$question->e}}" name="e" required>
                        </div>
                        <div class="form-group">
                            <label>True answer is</label>
                            <div class="row">
                                <?php $answer = ['a', 'b', 'c', 'd', 'e'] ?>
                                @foreach($answer as $a)
                                    <div class="col">
                                        <div class="form-check">
                                            <input {{$question->answer == $a ? 'checked' : ''}} value="{{$a}}" class="form-check-input" type="radio" name="answer" required>
                                            <label class="form-check-label">{{strtoupper($a)}}</label>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                        </div>
                    </div>

                    <div class="card-footer">
                        <a href="{{route('courses.exam', $question->course_id)}}">
                            <button type="button" class="btn btn-danger">Cancel</button>
                        </a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </section>
    </div>
@stop

@section('javascript')
    <script>
        CKEDITOR.replace('question', {
            height: 250
        });
    </script>
@stop
