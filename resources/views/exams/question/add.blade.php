@extends('template.app')

@section('title', 'Add Question')

@section('content')
    <div class="row">
        <section class="col">
            <div class="card">
                <form method="post" action="{{route('courses.exam.question.add.save')}}">
                    @csrf
                    <input value="{{$course_id}}" name="course_id" hidden>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Question</label>
                            <textarea required id="question" name="question"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="a">A Choice</label>
                            <input type="text" class="form-control" id="a" placeholder="Enter A choice here" name="a" required>
                        </div>
                        <div class="form-group">
                            <label for="b">B Choice</label>
                            <input type="text" class="form-control" id="b" placeholder="Enter B choice here" name="b" required>
                        </div>
                        <div class="form-group">
                            <label for="c">C Choice</label>
                            <input type="text" class="form-control" id="c" placeholder="Enter C choice here" name="c" required>
                        </div>
                        <div class="form-group">
                            <label for="d">D Choice</label>
                            <input type="text" class="form-control" id="d" placeholder="Enter D choice here" name="d" required>
                        </div>
                        <div class="form-group">
                            <label for="e">E Choice</label>
                            <input type="text" class="form-control" id="e" placeholder="Enter E choice here" name="e" required>
                        </div>
                        <div class="form-group">
                            <label>True answer is</label>
                            <div class="row">
                                <?php $answer = ['a', 'b', 'c', 'd', 'e'] ?>
                                @foreach($answer as $a)
                                    <div class="col">
                                        <div class="form-check">
                                            <input value="{{$a}}" class="form-check-input" type="radio" name="answer" required>
                                            <label class="form-check-label">{{strtoupper($a)}}</label>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                        </div>
                    </div>

                    <div class="card-footer">
                        <a href="{{route('courses.exam', $course_id)}}">
                            <button type="button" class="btn btn-danger">Cancel</button>
                        </a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </section>
    </div>
@stop

@section('javascript')
    <script>
        CKEDITOR.replace('question', {
            height: 250
        });
    </script>
@stop
