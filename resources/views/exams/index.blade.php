@extends('template.app')

@section('title', 'Courses list')

@section('content')
    <div class="row">
        <section class="col">
            <div class="card">
                <div class="card-body">
                    <table id="modules-table" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Course name</th>
                            <th>Total questions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($courses as $course)
                            <tr>
                                <td>
                                    <a href="{{route('courses.exam', $course['id'])}}">
                                        {{$course['name']}}
                                    </a>
                                </td>
                                <td>{{\App\Exam::where('course_id', $course->id)->count()}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- /.card-body -->
            </div>
        </section>
    </div>
@stop

@section('javascript')
    <script>
        $(function () {
            $("#modules-table").DataTable();
        });
    </script>
@stop
