@extends('template.app')

@section('title', 'Questions List')

@section('content')
    <div class="row">
        <section class="col">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-book mr-1"></i>
                    </h3>
                    <div class="card-tools">
                        <ul class="nav nav-pills ml-auto">
                            <li class="nav-item">
                                <a href="{{route('courses.exam.question.add', $course_id)}}">
                                    <button class="btn btn-success">
                                        <i class="fas fa-plus"></i>
                                        Add
                                    </button>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div><!-- /.card-header -->
                <div class="card-body">
                    <table id="questions-table" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Question</th>
                            <th>Answer</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($questions as $i=>$q)
                            <tr>
                                <td>{{$i+1}}</td>
                                <td>{{$q['question']}}</td>
                                <td>{{$q['answer']}}</td>
                                <td>
                                    <a href="{{route('courses.exam.question.delete', ['course_id'=>$course_id, 'id'=>$q['id']])}}">
                                        <button class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                    </a>
                                    <a href="{{route('courses.exam.question.edit', [$q['id']])}}">
                                        <button class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></button>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- /.card-body -->
            </div>
        </section>
    </div>
@stop

@section('javascript')
    <script>
        $(function () {
            $("#questions-table").DataTable();
        });
    </script>
@stop
