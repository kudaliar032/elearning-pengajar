<!-- Font Awesome -->
<link rel="stylesheet" href="{{asset('/plugins/fontawesome-free/css/all.min.css')}}" />
<!-- Ionicons -->
<link rel="stylesheet" href="{{asset('/plugins/ionicons/css/ionicons.min.css')}}" />
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}" />
<!-- Theme style -->
<link rel="stylesheet" href="{{asset('/adminlte/css/adminlte.min.css')}}" />
<!-- overlayScrollbars -->
<link rel="stylesheet" href="{{asset('/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}" />
<!-- Cropper.js -->
<link rel="stylesheet" href="{{asset('/plugins/cropperjs/dist/cropper.min.css')}}" />
<!-- Google Font: Source Sans Pro -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet" />
