<!-- Sidebar -->
<div class="sidebar">
    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                 with font-awesome or any other icon font library -->
            <li class="nav-item">
                <a href="{{route('dashboard.index')}}" class="nav-link {{request()->is('dashboard') ? 'active' : ''}}">
                    <i class="nav-icon fas fa-home"></i>
                    <p>Home</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('user.list')}}" class="nav-link {{request()->is('user') ? 'active' : ''}}">
                    <i class="nav-icon fas fa-user"></i>
                    <p>Users</p>
                </a>
            </li>
            <li class="nav-item has-treeview {{request()->is('courses*') ? 'menu-open' : ''}}">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-book"></i>
                    <p>
                        Courses
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{route('courses.list')}}" class="nav-link {{request()->is(['courses/add', 'courses/edit/*', 'courses']) ? 'active' : ''}}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Courses list</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('courses.modules')}}" class="nav-link {{request()->is('courses/module*') ? 'active' : ''}}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Modules</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('courses.exams')}}" class="nav-link {{request()->is('courses/exam*') ? 'active' : ''}}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Exams</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link" onclick="$('#logout-form').submit()">
                    <i class="nav-icon fas fa-sign-out-alt"></i>
                    <p>Logout</p>
                </a>
            </li>
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->

<form id="logout-form" method="post" action="{{route('logout')}}" style="display: none">
    @csrf
</form>
