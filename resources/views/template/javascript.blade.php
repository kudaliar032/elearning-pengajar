<!-- jQuery -->
<script src="{{asset('/plugins/jquery-3.4.1.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('/plugins/jquery-ui-1.12.1/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{asset('/plugins/bootstrap-4.4.1/js/bootstrap.bundle.min.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('/adminlte/js/adminlte.min.js')}}"></script>
<!-- Cropper.js -->
<script src="{{asset('/plugins/cropperjs/dist/cropper.min.js')}}"></script>
<!-- axios -->
<script src="{{asset('/plugins/axios/dist/axios.min.js')}}"></script>
<script>axios.defaults.headers.common['X-CSRF-TOKEN'] = "{{csrf_token()}}";</script>
<!-- bs-custom-file-input -->
<script src="{{asset('/plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        bsCustomFileInput.init();
    });
</script>
<!-- CKEditor 4 -->
<script src="{{asset('/plugins/ckeditor/ckeditor.js')}}"></script>
