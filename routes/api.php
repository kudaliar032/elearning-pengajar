<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Api')->prefix('v1')->group(function () {
    Route::get('/', function () { return "API"; });

    /*
     * Course API
     */
    Route::prefix('course')->middleware('auth.jwt')->group(function () {
        Route::get('list', 'CourseController@index');
        Route::get('list/not', 'CourseController@notMyCourse');
        Route::get('detail/{id}', 'CourseController@courseDetail');
    });

    /*
     * User API
     */
    Route::prefix('user')->middleware('auth.jwt')->group(function () {
        Route::post('take/course/{course_id}', 'UserController@takeCourse');
        Route::get('course/list', 'UserController@courseList');
        Route::get('detail', 'UserController@detail');
        Route::post('open/module/{course_id}/{module_id}', 'UserController@openModule');
        Route::get('course/score/{course_id}', 'UserController@courseScore');
        Route::post('detail/add', 'UserController@addDetail');
    });

    /*
     * Module API
     */
    Route::prefix('module')->middleware('auth.jwt')->group(function () {
        Route::get('list/{course_id}', 'ModuleController@getModuleList');
        Route::get('detail/{course_id}/{module_id}', 'ModuleController@getModule');
    });

    /*
     * Exam API
     */
    Route::prefix('exam')->middleware('auth.jwt')->group(function () {
        Route::get('questions/{course_id}', "ExamController@getQuestions");
        Route::post('answers/check', 'ExamController@answerCheck');
    });

    /*
     * Login & Register API
     */
    Route::post('login', 'AuthController@login');
    Route::middleware('auth.jwt')->post('login/verify', 'AuthController@verify');
    Route::post('register', 'AuthController@register');
});
