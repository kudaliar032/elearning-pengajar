<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('dashboard.index');
});

Route::name('dashboard.')->prefix('/dashboard')->middleware('auth')->group(function () {
    Route::get('/', 'DashboardController@index')->name('index');
});

Route::name('user.')->prefix('/user')->middleware('auth')->group(function () {
    Route::get('/', 'UserController@list')->name('list');

    Route::get('/add', 'UserController@add')->name('add');
    Route::post('/add', 'UserController@addSave')->name('add.save');

    Route::get('/edit/{id}', 'UserController@edit')->name('edit');
    Route::post('/edit', 'UserController@editSave')->name('edit.save');

    Route::get('/edit/photo/{id}', 'UserController@editPhoto')->name('edit.photo');
    Route::post('/edit/photo', 'UserController@editPhotoSave')->name('edit.photo.save');

    Route::get('/delete/{id}', 'UserController@delete')->name('delete');
});

Route::name('courses.')->prefix('/courses')->middleware('auth')->group(function () {
    Route::get('/', 'CoursesController@index')->name('list');

    Route::get('/add', 'CoursesController@add')->name('add');
    Route::post('/add/save', 'CoursesController@addSave')->name('add.save');
    Route::get('/edit/{id}', 'CoursesController@edit')->name('edit');
    Route::post('/edit/', 'CoursesController@editSave')->name('edit.save');
    Route::get('/edit/header/{id}', 'CoursesController@editHeader')->name('edit.header');
    Route::post('/edit/header', 'CoursesController@editHeaderSave')->name('edit.header.save');
    Route::get('/delete/{id}', 'CoursesController@delete')->name('delete');

    Route::get('/modules', 'ModulesController@index')->name('modules');
    Route::get('/modules/{course_id}', 'ModulesController@modulesList')->name('modules.list');
    Route::get('/module/add/{course_id}', 'ModulesController@add')->name('module.add');
    Route::post('/module/add/save', 'ModulesController@addSave')->name('module.add.save');
    Route::get('/module/edit/{id}', 'ModulesController@edit')->name('module.edit');
    Route::post('/module/edit/save', 'ModulesController@editSave')->name('module.edit.save');
    Route::get('/module/{course_id}/delete/{id}', 'ModulesController@delete')->name('module.delete');

    Route::get('/exams', 'ExamsController@index')->name('exams');
    Route::get('/exam/{course_id}', 'ExamsController@questions')->name('exam');
    Route::get('/exam/{course_id}/add', 'ExamsController@questionAdd')->name('exam.question.add');
    Route::post('/exam/question/add', 'ExamsController@questionAddSave')->name('exam.question.add.save');
    Route::get('/exam/{course_id}/question/delete/{id}', 'ExamsController@questionDelete')->name('exam.question.delete');
    Route::get('/exam/question/edit/{id}', 'ExamsController@questionEdit')->name('exam.question.edit');
    Route::post('/exam/question/edit-save', 'ExamsController@questionEditSave')->name('exam.question.edit.save');
});

Auth::routes();

Route::post('/login/pengajar', 'Auth\LoginController@loginPengajar')->name('login.pengajar');
